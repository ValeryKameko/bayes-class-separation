# bayes-class-separation

Class separation application using Bayes approach

## Documentation

To build and run:
```
yarn build
yarn start
```

To develop:
```
yarn watch
yarn start
```
