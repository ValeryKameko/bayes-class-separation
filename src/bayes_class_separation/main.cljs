(ns bayes_class_separation.main
  (:require ["electron" :refer [app BrowserWindow crashReporter]]
            ["path" :as path]
            ["url" :as url]))

(defn create-window []
  (let [win (BrowserWindow.
             #js {:width 800
                  :height 600
                  :webPreferences #js { :nodeIntegration true }
                  })
        url (url/format #js {:pathname (path/join js/__dirname "index.html")
                             :protocol "file:"
                             :slashes true})]
    (.loadURL win url)))

(defn main []
  (-> app (.whenReady) (.then create-window))

  (.on app "window-all-closed"
       #(when (not= js/process.platform "darwin")
          (.quit app)))

  (.on app "activate"
       #(if (= (-> BrowserWindow (.getAllWindows) (.-length)) 0)
          (create-window))))

