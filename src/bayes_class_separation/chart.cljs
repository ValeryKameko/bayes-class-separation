(ns bayes-class-separation.chart
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            ["recharts" :as recharts :refer [ComposedChart, Area, XAxis, YAxis, Line, ResponsiveContainer, Tooltip, ReferenceLine, Legend]]
            [thi.ng.color.core :as col]
            [bayes-class-separation.store :as store]))

(defn- stroke-color [index count]
  @(-> (col/hsva (/ index count) 1.0 1.0) col/as-int24 col/as-css))

(defn- fill-color [index count]
  @(-> (col/hsva (/ index count) 1.0 1.0 0.1) col/as-int24 col/as-css))

(defn- max-stroke-color [index count]
  @(-> (col/hsva (/ index count) 1.0 1.0) col/as-int24 col/as-css))

(defn- max-fill-color [index count]
  @(-> (col/hsva (/ index count) 1.0 1.0 0.1) col/as-int24 col/as-css))

(defn- area [index key count]
  ^{:key key}
  [:> Area {:type "monotone"
            :dataKey key
            :stroke (stroke-color index count)
            :fill (fill-color index count)}])

(defn- max-view [index key count]
  [^{:key key}
   [:> Area {:type "monotone"
             :dataKey key
             :stroke (max-stroke-color index count)
             :fill (max-fill-color index count)}]
   ^{:key (str key "-line")}
   [:> Line {:type "monotone"
             :strokeWidth 5
             :dataKey key
             :stroke (max-stroke-color index count)}]])

(defn- max-x-values [data]
  (let [adjacent-values (partition 2 1 data)
        filtered-values (filter #(not= (:max-key (first %)) (:max-key (second %))) adjacent-values)]
    (map #(:x (first %)) filtered-values)))

(defn- max-x-line [index x]
  ^{:key (str "x-line-" x)}
  [:> ReferenceLine {:x x
                     :stroke "green"
                     :strokeWidth 5}])

(defn ^:exports chart [samplers minx maxx]
  (let [data (store/sample-data-series samplers minx maxx)
        areas (map-indexed #(area %1 %2 (count samplers)) (keys samplers))
        max-views (apply concat (map-indexed #(max-view %1 (store/key-max %2) (count samplers)) (keys samplers)))
        max-x-lines (map-indexed max-x-line (max-x-values data))]
    [:> ResponsiveContainer {:aspect (/ 16 9)
                             :debounce 0.5}
     [:> ComposedChart {:data (clj->js data)
                        :margin { :top 20, :right 30, :left 0, :bottom 0 }}
      [:> XAxis { :dataKey :x }]
      [:> YAxis]
      [:> Tooltip]
      (concat areas max-views max-x-lines)]]))
