(ns bayes-class-separation.semantic-ui
  (:require [cljsjs.semantic-ui-react]
            [goog.object :as go]
            [clojure.string :as str]))

(def semantic-ui js/semanticUIReact)

(defn component [key]
  (let [keys (str/split key #"\.")]
    (apply go/getValueByKeys semantic-ui keys)))
