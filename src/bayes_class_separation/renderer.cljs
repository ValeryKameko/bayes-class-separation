(ns bayes_class_separation.renderer
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            ["react" :as react]
            [bayes-class-separation.semantic-ui :as sui]
            [bayes-class-separation.chart :as chart]
            [bayes-class-separation.store :as store]))

(def NumberInput (.-default (js/require "semantic-ui-react-numberinput")))

(defn- gaussian-component [key title]
  [:> (sui/component "Segment")
   [:> (sui/component "Header") {:as "h3"}
    title]
   [:> (sui/component "Form.Group")
    [:> (sui/component "Form.Field")
     [:label "Mu"]
     [:> NumberInput {:value (store/get-mu key)
                      :onChange #(store/set-mu key %)
                      :allowMouseWheel true
                      :valueType "decimal"
                      :stepAmount 0.5
                      :precision 3
                      :minValue -100
                      :maxValue 100
                      :size "mini"}]]]
   [:> (sui/component "Form.Group")
    [:> (sui/component "Form.Field")
     [:label "Sigma"]
     [:> NumberInput {:value (store/get-sigma key)
                      :onChange #(store/set-sigma key %)
                      :allowMouseWheel true
                      :valueType "decimal"
                      :stepAmount 0.5
                      :precision 2
                      :minValue 0.1
                      :maxValue 100
                      :size "mini"}]]]])

(defn- controls-component []
  (let [false-error (store/calc-false-error -1000 1000)
        miss-detection (store/calc-miss-detection-error -1000 1000)]
    [:> (sui/component "Segment.Group") {:style #js{:height "100vh"}}
     [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h2"
                                    :textAlign "center"}
       "Controls"]]
     [gaussian-component :gaussian1 "Gaussian 1"]
     [gaussian-component :gaussian2 "Gaussian 2"]
     [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h3"}
       "Class probability"]
      [:> (sui/component "Form.Group")
       [:> (sui/component "Form.Field")
        [:> NumberInput {:value (store/get-class-probability)
                         :onChange #(store/set-class-probability %)
                         :allowMouseWheel true
                         :valueType "decimal"
                         :stepAmount 0.01
                         :precision 3
                         :minValue 0
                         :maxValue 1
                         :size "mini"}]]]]
     [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h3"}
       "Probabilities"]
      [:> (sui/component "Header") {:as "h5"}
       (str "False error: " false-error)]
      [:> (sui/component "Header") {:as "h5"}
       (str "Miss detection: " miss-detection)]
      [:> (sui/component "Header") {:as "h5"}
       (str "Sum error: " (+ miss-detection false-error))]
      ]]))

(defn root-component []
  (let [gaussian-fn-1 (store/get-class-gaussian-fn :gaussian1)
        gaussian-fn-2 (store/get-class-gaussian-fn :gaussian2)]
    [:div
     [:> (sui/component "Grid") {:columns 2}
      [:> (sui/component "Grid.Row")
       [:> (sui/component "Grid.Column") {:width 12}
        [chart/chart {:gaussian-1 gaussian-fn-1
                      :gaussian-2 gaussian-fn-2} -100 100]]
       [:> (sui/component "Grid.Column") {:width 4}
        [controls-component]]]]]))

(rdom/render [root-component]
             (js/document.getElementById "app-container"))
