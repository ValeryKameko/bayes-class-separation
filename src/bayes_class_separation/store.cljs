(ns bayes-class-separation.store
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]))

(def ^:const samples-count 1000)

(def store (atom {:gaussian1 {:mu "-30"
                              :sigma "20"}
                  :gaussian2 {:mu "30"
                              :sigma "20"}
                  :class-probability "0.5"}))

(defn get-gaussian [gaussian]
  (gaussian @store))

(defn get-mu [gaussian]
  (:mu (get-gaussian gaussian)))

(defn get-sigma [gaussian]
  (:sigma (get-gaussian gaussian)))

(defn set-mu [gaussian value]
  (swap! store assoc-in [gaussian :mu] value))

(defn set-sigma [gaussian value]
  (swap! store assoc-in [gaussian :sigma] value))

(defn get-class-probability []
  (:class-probability @store))

(defn set-class-probability [value]
  (swap! store assoc :class-probability value))

(defn get-gaussian-fn [gaussian]
  (fn [x]
    (let [sigma (get-sigma gaussian)
          mu (get-mu gaussian)
          sigma2 (* sigma sigma)
          exponent (- (/ (Math/pow (- x mu) 2) (* 2 sigma2)))]
      (/ (Math/pow Math/E exponent)
         (Math/sqrt (* 2 Math/PI sigma2))))))

(defn get-class-gaussian-fn [gaussian]
  (let [class-probability (get-class-probability)
        class-multiplier (if (= gaussian :gaussian1)
                           class-probability
                           (- 1 class-probability))]
    #(* ((get-gaussian-fn gaussian) %) class-multiplier)))


(defn key-max [key]
  (symbol (str "max-" (name key))))

(defn keys-max [keys]
  (map key-max keys))

(defn sample-max-data [data-value]
  (let [max-empty (zipmap (keys-max (keys data-value)) (repeat nil))
        max-key-value (apply max-key second data-value)
        key (key-max (first max-key-value))]
    (conj max-empty (hash-map key 0 :max-key key))))

(defn sample-data [samplers x]
  (let [samples (into {} (for [[sampler-key sampler-fn] samplers] [sampler-key (sampler-fn x)]))
        max-samples (sample-max-data samples)]
    (conj {:x x} samples max-samples)))

(defn sample-data-series [samplers minx maxx]
  (let [x-series (map #(+ (* (/ % samples-count) (- maxx minx)) minx)
                      (range samples-count))]
    (map #(sample-data samplers %) x-series)))

(defn false-error-fn [data-value key1 key2]
  (let [value1 (key1 data-value)
        value2 (key2 data-value)]
    (if (> value1 value2)
      value2
      0)))

(defn miss-detection-fn [data-value key1 key2]
  (let [value1 (key1 data-value)
        value2 (key2 data-value)]
    (if (> value2 value1)
      value1
      0)))

(defn- calc-error [func minx maxx]
  (let [samplers {:sampler-fn-1 (get-class-gaussian-fn :gaussian1)
                  :sampler-fn-2 (get-class-gaussian-fn :gaussian2)}
        data-series (sample-data-series samplers minx maxx)
        func-series (map #(func % :sampler-fn-1 :sampler-fn-2) data-series)]
    (-> (reduce + func-series)
        (/ samples-count)
        (* (- maxx minx)))))

(defn calc-false-error [minx maxx]
  (calc-error false-error-fn minx maxx))

(defn calc-miss-detection-error [minx maxx]
  (calc-error miss-detection-fn minx maxx))
